class ApplicationController < ActionController::Base
  before_action :set_genres, :set_authors

  private
    def set_genres
      @genres = Genre.all
    end
    def set_authors
      @authors = Author.all
    end
end
