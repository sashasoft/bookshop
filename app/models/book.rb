class Book < ApplicationRecord
  belongs_to :author
  belongs_to :genre
  monetize :price_cents, as: :price
  include PgSearch::Model
  
  pg_search_scope :search,
                  against: [:title, :description, :author_id, :genre_id],
                  using: { tsearch: {dictionary: "english"} },
                  associated_against: { 
                    author: [:first_name, :last_name],
                    genre: [:name]
                  }

end
