class Genre < ApplicationRecord
  has_many :books
  include PgSearch::Model
end
