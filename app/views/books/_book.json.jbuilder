json.extract! book, :id, :title, :description, :genre, :img_url, :author_id, :created_at, :updated_at
json.url book_url(book, format: :json)
