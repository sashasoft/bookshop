Rails.application.routes.draw do
  devise_for :users
  
  root "books#index"
  resources :genres
  
  get 'search', to: 'search#index'
  resources :authors
  resources :books

end
