class AddPriceCenstToBooks < ActiveRecord::Migration[7.0]
  def change
    add_column :books, :price_cents, :integer
  end
end
