# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

10.times do
  author = Author.create(first_name: FFaker::Name.first_name,
                    last_name: FFaker::Name.last_name)
  genre = Genre.create(name: FFaker::Book.genre)
  5.times do
    Book.create(title: FFaker::Book.title,
                description: FFaker::Book.description,
                genre_id: genre.id,
                img_url: FFaker::Book.cover,
                author_id: author.id,
                price_cents: rand(10000))
  end
end
